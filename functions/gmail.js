// express-oauth is a Google-provided, open-source package that helps automate
// the authorization process.
const Auth = require('@google-cloud/express-oauth2-handlers');
// googleapis is the official Google Node.js client library for a number of
// Google APIs, including Gmail.
const {google} = require('googleapis');
const query = 'from:trackingupdates@fedex.com subject:"FedEx Tracking" in:anywhere newer_than:4y';
const gmail = google.gmail('v1');

// Specify the access scopes required. If authorized, Google will grant your
// registered OAuth client access to your profile, email address, and data in
// your Gmail and Google Sheets.
const requiredScopes = [
  'profile',
  'email',
  'https://www.googleapis.com/auth/gmail.readonly'
];

const auth = Auth('datastore', requiredScopes, 'email', true);

// Call the Gmail API (Users.watch) to set up Gmail push notifications.
// Gmail will send a notification to the specified Cloud Pub/Sun topic
// every time a new mail arrives in inbox.
const getGmailMessages = (email, query) => {
  gmail.users.messages.list({
    userId: email,
	  //maxResults: 1,
    q: query
  }, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
	
    const messages = res.data.messages;
	
    console.log(messages.length + ' mails found.');
    console.log('Message IDs: ');
	
    if (messages.length) {
      messages.forEach((message) => {
		
		// Get the message id which we will need to retreive tha actual message next.
        console.log(`- ${message.id}`);
		
		// Retreive the actual message using the message id
		gmail.users.messages.get({
      userId: email,
      'id': message.id
    }, function(err, response) {
			if (err) {
				console.log('The API returned an error: ' + err);
				return;
			}
		    message_raw = response.data.payload.parts[0].body.data;
			buff = Buffer.from(message_raw, 'base64'); 
			message_text = buff.toString('ascii');
			
			const $ = cheerio.load(message_text);
			console.log();
			console.log('Tracking Number: ' + $('.mobile .mobilepadded tr:nth-child(4)').text().match('[0-9]+')[0].trim());
			console.log('Ship Date: ' + $('.mobile .mobilepadded table td:first-child tr:nth-child(2)').text().trim());
			console.log('From: ' + $('.mobile .mobilepadded table td:first-child tr:last-child').text().split(',')[0].trim());
			console.log('To: ' + $('.mobile .mobilepadded table td:last-child tr:last-child').text().split(',')[0].trim());

		});
      });
    } else {
      console.log('No mails found.');
    }
  });
};

// If the authorization process completes successfully, set up Gmail push
// notification using the tokens returned
const onSuccess = async (req, res) => {
  let email;

  try {
    // Set up the googleapis library to use the returned tokens.
    email = await auth.auth.authedUser.getUserId(req, res);
    const OAuth2Client = await auth.auth.authedUser.getClient(req, res, email);
    google.options({auth: OAuth2Client});
  } catch (err) {
    console.log(err);
    throw err;
  }

  try {
    await getGmailMessages(email, query);
  } catch (err) {
    console.log(err);
    /*if (!err.toString().includes('one user push notification client allowed per developer')) {
      throw err;
    }*/
  }

  res.send(`Successfully set up Gmail push notifications.`);
};

// If the authorization process fails, return an error message.
const onFailure = (err, req, res) => {
  console.log(err);
  res.send(`An error has occurred in the authorization process.`);
};

// Export the Cloud Functions for authorization.
exports.auth_init = auth.routes.init;
exports.auth_callback = auth.routes.cb(onSuccess, onFailure);
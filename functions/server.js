var request = require('request');
const axios = require('axios');
const carrier = 'fedex';
var tracking_number = 102321727980;
var postData, url;

var url = '';

function sentRes(url,data,method,fn){
    data=data||null;
    var content;
    if(data==null){
        content=require('querystring').stringify(data);
    }else{
        content = JSON.stringify(data); //json format
    }

    var options={
        url: url,
		method: method,
        headers:{
            'Content-Type':'application/json',
            'Content-Length':Buffer.byteLength(content,"utf8"),
            'Trackingmore-Api-Key':'feffef58-77b3-4a6b-93cc-50c20ccd09b3'
        }
    };
    return new Promise( function( resolve, reject ){
		var req = axios(options)
		.then((response) => {
			//agent.add(response.data);
			fn!=undefined && fn(response.data);
			//console.log(response.data);
			resolve();
		})
		.catch(function (error) {
			console.log(error);
			reject(error);
		})
		.then(function () {
			// always executed
			console.log(`Listing Completed.`);
		});
	});
  }

postData = null;
    
url = 'http://api.trackingmore.com/v2/trackings/' + carrier + '/' + tracking_number + '/en';
return new Promise( function( resolve, reject ){
	sentRes(url,postData,"GET",function(resp){
		console.log(JSON.stringify(resp.data));
		console.log("Status is " + resp.data.status + ", Updated at " + resp.data.lastUpdateTime);
		resolve(resp.data);
	});
});

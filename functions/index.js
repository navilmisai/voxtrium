// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';

const fs = require('fs');
const readline = require('readline');
const cheerio = require('cheerio');
const functions = require('firebase-functions');
const { WebhookClient } = require('dialogflow-fulfillment');
const { Card, Suggestion } = require('dialogflow-fulfillment');
const { google } = require('googleapis');
const { dialogflow, BasicCard, Permission, Table, List, Button,
    Suggestions, Carousel, Image } = require('actions-on-google');
const axios = require('axios');
const carrier = 'fedex';
var tracking_number = 102321727980;
var postData, url;
const userId = 'banureka.vitto@gmail.com';
const query = 'from:trackingupdates@fedex.com subject:"FedEx Tracking" in:anywhere newer_than:4y';
var o = {};
var key = 'Messages';
o[key] = [];
var statusResult;
    
process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements

const gmail = google.gmail('v1');

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/gmail.readonly'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
//var tokenstring = '{"access_token":"ya29.a0Adw1xeXCec8Y3Ubid7j8_HGQsifi1QRRXSwA1iGOs6Hkb4WhhZUSrA6-PIWzBuvCWcAvL8uHgkQXU52ozTjnYIxVGmjrAM5WMrV8eAPwmtMfQPMfZ1qNnanZA8BTFo2xDMSqMvHBPVBr63fO_5crnvDj16B7KVxlFBM","refresh_token":"1//0gPId4KsGehtcCgYIARAAGBASNwF-L9IrS2GFtAj70GbEB6ZwSlzDBFPtJOev76ni9r-dsSN7d5T8MSKx2GWtJDnCqD-ygtGzGVQ","scope":"https://www.googleapis.com/auth/gmail.readonly","token_type":"Bearer","expiry_date":1582640056374}';
const TOKEN_PATH = 'token.json';

/*fs.writeFile(TOKEN_PATH, tokenstring, (err) => {
  if (err) return console.error(err);
  //console.log('Initial Token stored to', TOKEN_PATH);
});*/

var clientSecret='{"installed":{"client_id":"461598935612-c2nkmki6qd0hdet28b7tpddm9ah3viej.apps.googleusercontent.com","project_id":"voxidoxi-myyebg","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://oauth2.googleapis.com/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"AmHy9u-l8tycUbv_EKw7aFRX","redirect_uris":["urn:ietf:wg:oauth:2.0:oob","http://localhost"]}}';

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials) {
  return new Promise( function( resolve, reject ){
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
      if (err) return getNewToken(oAuth2Client);
      oAuth2Client.setCredentials(JSON.parse(token));
      //callback(oAuth2Client);
      resolve(oAuth2Client);
    });
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client) {
  return new Promise( function( resolve, reject ){
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    //var code = '4/wwFawt9-4NglWBZDjNVct8H1aK9B2ELdhZyfcRoR2VYDudNVC0Pp4D0';
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      //callback(oAuth2Client);
      resolve(oAuth2Client);
    });
  });
});
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
	const agent = new WebhookClient({ request, response });
	console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
	console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
  
  /**
  * Get the recent email from your Gmail account
  *
  * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
  */
  function alistMessages(auth) {
    var o = {};
	var key = 'Messages';
	o[key] = [];
    const gmail = google.gmail({ version: 'v1', auth });
    //return new Promise( function( resolve, reject ){
    gmail.users.messages.list({
      userId: userId,
      //maxResults: 1,
      q: query
    }, (err, res) => {
      if (err) return console.log('The API returned an error: ' + err);

      const messages = res.data.messages;

      //console.log(messages.length + ' mails found.');
      //console.log('Message IDs: ');

      if (messages.length) {
        messages.forEach((message) => {

          // Get the message id which we will need to retreive tha actual message next.
          //console.log(`- ${message.id}`);

          // Retreive the actual message using the message id
          gmail.users.messages.get({ auth: auth, userId: userId, 'id': message.id }, function (err, response) {
            if (err) {
              console.log('The API returned an error: ' + err);
              return;
            }

            var message_raw = response.data.payload.parts[0].body.data;
            var buff = Buffer.from(message_raw, 'base64');
            var message_text = buff.toString('ascii');

            const $ = cheerio.load(message_text);

            var tracking_data = {
              message_id: message.id,
              tracking_number: $('.mobile .mobilepadded tr:nth-child(4)').text().match('[0-9]+')[0].trim(),
              ship_date: $('.mobile .mobilepadded table td:first-child tr:nth-child(2)').text().trim(),
              from: $('.mobile .mobilepadded table td:first-child tr:last-child').text().split(',')[0].trim(),
              to: $('.mobile .mobilepadded table td:last-child tr:last-child').text().split(',')[0].trim()
            };
            
            o[key].push(tracking_data);
            console.log(o.Messages.length);

          });
        });
      } else {
        agent.add('No mails found.');
        return;
      }
      //resolve(o);
      
    });
    agent.add(o.Messages.length + ' mails found.');
  //});
  }

	function welcome(agent) {
		agent.add(`Welcome to Voxi Doxi!`);
	}
	
	function fallback(agent) {
		agent.add(`I didn't understand`);
		agent.add(`I'm sorry, can you try again?`);
	}
	
	async function sentRes(url,data,method){
		//return new Promise( function( resolve, reject ){
    data=data||null;
		var content;
		if(data==null){
			content=require('querystring').stringify(data);
		}else{
			content = JSON.stringify(data); //json format
		}
		
		var options={
			url: url,
			method: method,
			headers:{
				'Content-Type':'application/json',
				'Content-Length':Buffer.byteLength(content,"utf8"),
				'Trackingmore-Api-Key':'661724ac-ade2-406c-a8ed-f3e3db1124f6'
			}
		};
		
    var req = await axios(options)
		.then((response) => {
      //callback!=undefined && callback(response.data);
      console.log(response.data);
      statusResult = response.data;
      //return response.data;
      //resolve(response.data);
		})
		.catch(function (error) {
      console.log(error);
      //reject(error);
		})
		.then(function () {
			// always executed
			console.log(`Listing Shipment status Completed.`);
    });
    //});
	}
	
	// // Uncomment and edit to make your own intent handler
	// // uncomment `intentMap.set('your intent name here', yourFunctionHandler);`
	// // below to get this function to be run when a Dialogflow intent is matched
	function yourFunctionHandler(agent) {
		agent.add(`This message is from Dialogflow's Cloud Functions for Firebase editor!`);
		agent.add(new Card({
			title: `Title: this is a card title`,
			imageUrl: 'https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png',
			text: `This is the body text of a card.  You can even use line\n  breaks and emoji! 💁`,
			buttonText: 'This is a button',
			buttonUrl: 'https://assistant.google.com/'
			})
		);
		agent.add(new Suggestion(`Quick Reply`));
		agent.add(new Suggestion(`Suggestion`));
		agent.setContext({ name: 'weather', lifespan: 2, parameters: { city: 'Rome' }});
	}
	
  async function mailHandler(agent) {
    	
  }
  
  async function shipmentHandler(agent) {

    postData = null;

    var selected = agent.parameters.selected;
    tracking_number = o.Messages[selected].tracking_number;
    
    console.log("User selected: " + selected);
    console.log("Tracking number: " + tracking_number);

    url = 'http://api.trackingmore.com/v2/trackings/' + carrier + '/' + tracking_number + '/en';

    await sentRes(url, postData, "GET");

    agent.add("Status is " + statusResult.data.status + ", Updated at " + statusResult.data.lastUpdateTime);

    agent.add(new Card({
      title: `Shipment History`,
      imageUrl: 'https://www.reveelgroup.com/wp-content/uploads/2019/09/Depositphotos_194206346_xl-2015-min-1-e1567533029343.jpg',
      text: `Click the below link to check the shipment history.`,
      buttonText: 'Visit FedEx website',
      buttonUrl: 'https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber=' + tracking_number
    }));
    //agent.add(new Suggestion(`Cancel`));

  }
  
	async function voxiIntentHandler(agent) {
      
      	
      
		//agent.add(`Checking Shipment details... Please wait.`);
		
    var authPromise = authorize(JSON.parse(clientSecret));
    //console.log('Auth Promise: ' + authPromise);
    var promiseMails = authPromise.then(async function (auth) {
      const gmail = google.gmail({ version: 'v1', auth });
      //return new Promise( function( resolve, reject ){
      let mails = await gmail.users.messages.list({
        userId: userId,
        //maxResults: 1,
        q: query
      });
      /*, (err, res) => {
        if (err) return console.log('The API returned an error: ' + err);
      */
        var messages = mails.data.messages; //res

        return new Promise (async (resolve, reject) => {
        if (messages) {
            //messages.forEach(async (message) => {
            
            await asyncForEach(messages, async(message) => {

            // Retreive the actual message using the message id
              let onemail = await gmail.users.messages.get({ auth: auth, userId: userId, 'id': message.id });
              /*, function (err, response) {
              if (err) {
                console.log('The API returned an error: ' + err);
                return;
              }*/
  
              var message_raw = onemail.data.payload.parts[0].body.data; //response
              var buff = Buffer.from(message_raw, 'base64');
              var message_text = buff.toString('ascii');
  
              //const promisedCheerio = util.promisify(cheerio.load);
              const $ = cheerio.load(message_text);
  
              var tracking_data = {
                message_id: message.id,
                tracking_number: $('.mobile .mobilepadded tr:nth-child(4)').text().match('[0-9]+')[0].trim(),
                ship_date: $('.mobile .mobilepadded table td:first-child tr:nth-child(2)').text().trim(),
                from: $('.mobile .mobilepadded table td:first-child tr:last-child').text().split(',')[0].trim(),
                to: $('.mobile .mobilepadded table td:last-child tr:last-child').text().split(',')[0].trim()
              };
              o[key].push(tracking_data);
              //console.log('inside foreach' + o[key]);
            });
          //});
          resolve(o);
          //console.log('outside foreach' + o[key]);
        } else {
          //agent.add('No mails found.');
          reject('no shipment found');
        }
      });
        //resolve(o);
      //});
    });

		/*postData = {
			"tracking_number": tracking_number,
			"carrier_code": carrier
		};
		
		url = 'http://api.trackingmore.com/v2/trackings/post';
		sentRes(url,postData,"post",function(data){
			console.log(data);
    });*/
    
    await promiseMails.then(function(result) {
      var mcount;
      if (o.Messages.length > 1) {
        mcount = o.Messages.length;
      	agent.add("I have found " + mcount + " fedex shipments. Please choose any one of them to check the status:");
      	var msgDetail ='';
      	o.Messages.forEach((msg, index) => {
        	msgDetail += ' Package ' + (index+1) + ': from - ' + msg.from + ', To - ' + msg.to + ', sent on ' + msg.ship_date +'.';
      	});
      	return agent.add(msgDetail);
      }
      else if(o.Messages.length == 1)
        return agent.add(' Package from - ' + o.Messages[0].from + ', To - ' + o.Messages[0].to + ', sent on ' + o.Messages[0].ship_date);
    })
    .catch(function(result) {
      console.log(result);
      return agent.add("No mails found.");
    });
  }
  
	// test locally:
	// firebase functions:shell
	// dialogflowFirebaseFulfillment({ method: 'POST', json: true, body: require("../testdata-others.json") });
	
	// // Uncomment and edit to make your own Google Assistant intent handler
	// // uncomment `intentMap.set('your intent name here', googleAssistantHandler);`
	// // below to get this function to be run when a Dialogflow intent is matched
	// function googleAssistantHandler(agent) {
	//   let conv = agent.conv(); // Get Actions on Google library conv instance
	//   conv.ask('Hello from the Actions on Google client library!') // Use Actions on Google library
	//   agent.add(conv); // Add Actions on Google library responses to your agent's response
	// }
	// // See https://github.com/dialogflow/fulfillment-actions-library-nodejs
	// // for a complete Dialogflow fulfillment library Actions on Google client library v2 integration sample
	
	// Run the proper function handler based on the matched Dialogflow intent name
	let intentMap = new Map();
	intentMap.set('Default Welcome Intent', welcome);
	intentMap.set('Default Fallback Intent', fallback);
  intentMap.set('Initial Intent', voxiIntentHandler);
  intentMap.set('Check Mails', mailHandler);
	intentMap.set('Initial Intent - select.number', shipmentHandler);
	//intentMap.set('your intent name here', googleAssistantHandler);
	agent.handleRequest(intentMap);
});
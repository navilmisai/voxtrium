const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const cheerio = require('cheerio');

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/gmail.readonly'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
var tokenstring = '{"access_token":"ya29.a0Adw1xeXCec8Y3Ubid7j8_HGQsifi1QRRXSwA1iGOs6Hkb4WhhZUSrA6-PIWzBuvCWcAvL8uHgkQXU52ozTjnYIxVGmjrAM5WMrV8eAPwmtMfQPMfZ1qNnanZA8BTFo2xDMSqMvHBPVBr63fO_5crnvDj16B7KVxlFBM","refresh_token":"1//0gPId4KsGehtcCgYIARAAGBASNwF-L9IrS2GFtAj70GbEB6ZwSlzDBFPtJOev76ni9r-dsSN7d5T8MSKx2GWtJDnCqD-ygtGzGVQ","scope":"https://www.googleapis.com/auth/gmail.readonly","token_type":"Bearer","expiry_date":1582640056374}';
const TOKEN_PATH = 'token.json';

fs.writeFile(TOKEN_PATH, tokenstring, (err) => {
  if (err) return console.error(err);
  //console.log('Initial Token stored to', TOKEN_PATH);
});

var clientSecret='{"installed":{"client_id":"461598935612-c2nkmki6qd0hdet28b7tpddm9ah3viej.apps.googleusercontent.com","project_id":"voxidoxi-myyebg","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://oauth2.googleapis.com/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"AmHy9u-l8tycUbv_EKw7aFRX","redirect_uris":["urn:ietf:wg:oauth:2.0:oob","http://localhost"]}}';
// Load client secrets from a local file.
//fs.readFile('client_secret.json', (err, content) => {
  //if (err) return console.log('Error loading client secret file:', err);
  //// Authorize a client with credentials, then call the Gmail API.
  authorize(JSON.parse(clientSecret), listMessages);
//});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'online',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

/**
 * Get the recent email from your Gmail account
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listMessages(auth) {
  const gmail = google.gmail({version: 'v1', auth});
  gmail.users.messages.list({
    userId: 'me',
	//maxResults: 1,
    q: 'from:trackingupdates@fedex.com subject:"FedEx Tracking" in:anywhere newer_than:4y'
  }, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
	
    const messages = res.data.messages;
	
	console.log(messages.length + ' mails found.');
	console.log('Message IDs: ');
	
    if (messages.length) {
      messages.forEach((message) => {
		
		// Get the message id which we will need to retreive tha actual message next.
        console.log(`- ${message.id}`);
		
		// Retreive the actual message using the message id
		gmail.users.messages.get({auth: auth, userId: 'me', 'id': message.id}, function(err, response) {
			if (err) {
				console.log('The API returned an error: ' + err);
				return;
			}
		    message_raw = response.data.payload.parts[0].body.data;
			buff = Buffer.from(message_raw, 'base64'); 
			message_text = buff.toString('ascii');
			
			const $ = cheerio.load(message_text);
			console.log();
			console.log('Tracking Number: ' + $('.mobile .mobilepadded tr:nth-child(4)').text().match('[0-9]+')[0].trim());
			console.log('Ship Date: ' + $('.mobile .mobilepadded table td:first-child tr:nth-child(2)').text().trim());
			console.log('From: ' + $('.mobile .mobilepadded table td:first-child tr:last-child').text().split(',')[0].trim());
			console.log('To: ' + $('.mobile .mobilepadded table td:last-child tr:last-child').text().split(',')[0].trim());

		});
      });
    } else {
      console.log('No mails found.');
    }
  });
}